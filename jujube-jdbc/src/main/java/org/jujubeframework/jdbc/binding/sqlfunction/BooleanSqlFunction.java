package org.jujubeframework.jdbc.binding.sqlfunction;

/**
 * boolean判断函数处理
 *
 * @author John Li
 */
public interface BooleanSqlFunction extends SqlFunction {
}
